/*jshint esversion: 6 */

/* 
 * Script name: man.js
 * Author: Gervel Giva
 * Description:
 *  A small script that will regularly check the menu availabiliy of the specified store.
 *  A desktop notification will show if one of the items in the menu becomes unavaible for any reason.
 *
 *  This scripts assumes that jQuery is already embedded in the page.
 *
 * Version: 1.0.2
 */

(() => {
    'use strict';

    const REQUIRED_MENU_COUNT = 3;
    const MENU_CHECK_INTERVAL = 1000 * 10; // 1000 ms x 10 seconds
    const RELOAD_INTERVAL = 1000 * 60 * 15; // 1000 ms x 60 seconds x 15 minutes
    const ALERT_DELAY = 1000;
	const startTimeHour = 12
	const endTimeHour = 20


    const MENU_ITEM_DOM = '.dish-name span';

    const ICON_URL = 'https://img.icons8.com/dusk/64/000000/warning-shield.png'; const NOTIFICATION_TITLE = 'Menu Unavailable!';
    const NOTIFICATION_BODY = 'Some items in the menu is no longer available! Check the app to confirm this change.';

    let CHECKER_ID = null;
    let RELOAD_ID = null;

    const UI_STYLE = `
        position: fixed; 
        top: 5em; 
        left: 1em;
        z-index: 9999;
    `;

    const REQUEST_BTN_ID = 'requestNotification';
    const DISABLE_BTN_ID = 'disableChecker';
    
    const UI = `
        <div style="${UI_STYLE}">
            <button id="${DISABLE_BTN_ID}">Disable Checker</button>
            <br>
            <button id="${REQUEST_BTN_ID}">Enable Notifications</button>    
        </div>
    `;

    init();

    function init() {
        console.log('[START] Starting MAN script....');
		
		const STORE_ID = 'q8rm';
		if(location.pathname.indexOf(STORE_ID) > -1) {
			addUI();
			$('body').on('click', `#${REQUEST_BTN_ID}`, requestNotification);
			$('body').on('click', `#${DISABLE_BTN_ID}`, disableChecker);
			CHECKER_ID = setTimeout(menuChecker, MENU_CHECK_INTERVAL);

			console.log('[WAITING] Initialization complete. Waiting for events...');
		} else {
			console.log('[INFO] Current page is not the specified store');
		}	
    }

    function addUI() {
        if (!('Notification' in window)) {
            console.error('[ERROR] This browser does not support desktop notification');
            return;
        }

        console.log('[INFO] Adding UI...');
        $('body').prepend(UI);

        if(Notification.permission === 'granted') {
            console.log(`[INFO] Notification already allowed. Hiding request button...`);
            $(`#${REQUEST_BTN_ID}`).hide();
        } else {
            console.log('[INFO] Permission: ' + Notification.permission);
        }
        
    }

    function requestNotification(e) {
        console.log('[INFO] Requesting permission to use Notification...');
        Notification.requestPermission().then(function(permission) {
            // If the user accepts, let's create a notification
            if (permission === 'granted') {
                console.log('[INFO] Notification granted!');
                $(`#${REQUEST_BTN_ID}`).hide();
            } else {
                console.error('[ERROR] Notification was not allowed. Notification permission: ' + Notification.permission);
            }
        });
    }

    function disableChecker() {
        console.log(`[INFO] Disabled menu checker.`);
        clearTimeout(CHECKER_ID);
        clearTimeout(RELOAD_ID);
    }

    function spawnNotification() {
		let options = {
            body: NOTIFICATION_BODY,
            icon: ICON_URL,
            requireInteraction: true
        }
		
        new Notification(NOTIFICATION_TITLE, options);
        if (Notification.permission !== 'granted') {
            alert(NOTIFICATION_TITLE);
			
			setTimeout(reload, RELOAD_INTERVAL);
            return;
        }
	
    }

    function menuChecker() {
		const currentHour = new Date().getHours();
		if (currentHour < startTimeHour || currentHour > endTimeHour) {
            console.log('[INFO] Not the right time to run this script yet...');
            RELOAD_ID = setTimeout(reload, RELOAD_INTERVAL);
			return;
		}

		console.log('[INFO] Menus items available: ', $(MENU_ITEM_DOM).length);
        if (requiredMenuIsAvailable()) {
            console.log('[INFO] Menu available! Page reload will be performed in a few minutes...');
            console.log(`[INFO] Date: ${new Date()}`);
            RELOAD_ID = setTimeout(reload, RELOAD_INTERVAL);
        } else {
            console.error('[ERROR] NOT Available!');
			console.log(`[INFO] Date: ${new Date()}`);
			setTimeout(reload, RELOAD_INTERVAL);
            spawnNotification();
        }
    }

    function reload() {
        console.log(`[INFO] Reloading page...`);
        location.reload();
    }

    function requiredMenuIsAvailable() {
        return $(MENU_ITEM_DOM).length >= REQUIRED_MENU_COUNT;
    }

})();
